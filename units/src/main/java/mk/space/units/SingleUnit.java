package mk.space.units;

import lombok.AllArgsConstructor;
import lombok.Getter;
import mk.space.units.exception.EmptyElementOperation;
import mk.space.units.exception.UnitMismatchOperation;

public abstract class SingleUnit {

    private static final SingleUnit unitless = new Unitless();
    
    public static SingleUnit of(String unit) {
        return new Unitfull(unit, 1);
    }
    public static SingleUnit unitless() {
        return unitless;
    }
    
    public abstract boolean applicableForOperation(SingleUnit unit);
    public abstract SingleUnit multiply(SingleUnit unit);
    public SingleUnit divide(SingleUnit unit) {return multiply(unit.inversion());}
    public abstract SingleUnit inversion();
    
    public abstract boolean hasUnit();
    public abstract String getSymbol();
    public abstract int getExponent();

    @AllArgsConstructor
    private static class Unitfull extends SingleUnit {

        @Getter
        private final String symbol;

        @Getter
        private final int exponent;

        @Override
        public boolean hasUnit() {
            return true;
        }

        @Override
        public boolean applicableForOperation(SingleUnit unit) {
            return !unit.hasUnit() || this.getSymbol().equals(unit.getSymbol());
        }

        @Override
        public SingleUnit multiply(SingleUnit unit) {
            if (!applicableForOperation(unit)) {
                throw new UnitMismatchOperation();
            }
            return (this.exponent + unit.getExponent() == 0)
                    ? SingleUnit.unitless()
                    : new Unitfull(this.symbol, this.exponent+unit.getExponent());
        }

        @Override
        public SingleUnit inversion() {
            return new Unitfull(this.symbol, -this.exponent);
        }

        @Override
        public String toString() {
            return "[" + symbol + ((exponent == 1) ? "" : "^" + exponent) + "]";
        }
    }

    private static class Unitless extends SingleUnit {
        @Override
        public boolean applicableForOperation(SingleUnit unit) {
            return true;
        }

        @Override
        public SingleUnit multiply(SingleUnit unit) {
            return unit;
        }

        @Override
        public SingleUnit inversion() {
            return this;
        }

        @Override
        public boolean hasUnit() {
            return false;
        }

        @Override
        public String getSymbol() {
            throw new EmptyElementOperation();
        }

        @Override
        public int getExponent() {
            throw new EmptyElementOperation();
        }

        @Override
        public String toString() {
            return "[]";
        }
    }
}
