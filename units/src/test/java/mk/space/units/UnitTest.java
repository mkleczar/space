package mk.space.units;

import org.junit.Test;

import static org.junit.Assert.*;

public class UnitTest {
    @Test
    public void createUnits() {
        SingleUnit dist = SingleUnit.of("m");
        SingleUnit time = SingleUnit.of("s");
        //Unit velo = dist.dividedBy(time);
        //assertEquals("velocity unit m/s", "m/s", velo.toString());
    }

    @Test
    public void squareUnits() {
        SingleUnit mass1 = SingleUnit.of("g");
        SingleUnit mass2 = SingleUnit.of("g");
        SingleUnit mass = mass1.multiply(mass2);
        assertEquals("mass squared", "[g^2]", mass.toString());
    }

    @Test
    public void createUnitless() {
        SingleUnit dist1 = SingleUnit.of("m");
        SingleUnit dist2 = SingleUnit.of("m");
        SingleUnit unitless = dist1.divide(dist2);
        assertFalse("value without unit", unitless.hasUnit());
        assertEquals("unitless value", "[]", unitless.toString());
    }
}
